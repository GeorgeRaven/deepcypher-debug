FROM archlinux:latest

ARG CONTAINER_APP_DIR="/app"

COPY "./app/" "${CONTAINER_APP_DIR}"
COPY "./LICENSE" "${CONTAINER_APP_DIR}/LICENSE"

# resetting all gnupg keys
RUN rm -rf /etc/pacman.d/gnupg && \
    pacman-key --init && \
    pacman-key --populate archlinux

# Install core archlinux keyring and base-devel since we deleted keys
RUN pacman -Syyuu \
    archlinux-keyring \
    base-devel \
    --noconfirm

# install debugging tools/ tools we might want to test everything
RUN pacman -S \
    bind \
    fish \
    --noconfirm

# copy over all files if any
RUN cd "${CONTAINER_APP_DIR}" && \
    ls -al --color && \
    echo "Hello, debug-container here."

# drop into shell
CMD fish
